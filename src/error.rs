use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub enum Error {
    NeedMoreInput,
    InvalidHttpMethod,
    InvalidHttpVersion,
    InvalidHeaderKey,
    Fail(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NeedMoreInput => f.write_str("Need more input"),
            Self::InvalidHttpMethod => f.write_str("Invalid HTTP Method"),
            Self::InvalidHttpVersion => f.write_str("Invalid HTTP Version"),
            Self::InvalidHeaderKey => f.write_str("Invalid HTTP Heaader Key"),
            Self::Fail(msg) => write!(f, "Failure while parsing: {}", msg),
        }
    }
}

impl std::error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;
