//! HTTP Request method
//!
//! [The request method](https://datatracker.ietf.org/doc/html/rfc7231#section-4.3) is used to indicate the purpose of a HTTP request.
//!
//! # Examples
//!
//! ```
//! use h1_rs::method::Method;
//!
//! assert_eq!(Method::Get.as_ref(), "GET");
//! ```

use std::str::FromStr;

#[derive(Debug, Clone, Copy, Hash)]
pub enum Method {
    Get,
    Head,
    Post,
    Put,
    Delete,
    Connect,
    Options,
    Trace,
    /// [https://datatracker.ietf.org/doc/html/rfc5789](https://datatracker.ietf.org/doc/html/rfc5789)
    Patch,
}

impl Method {
    /// is_safe returns true if the semantics for a HTTP method are essentially
    /// read-only, and the client does not expect any state change on the
    /// server as a result of the request.
    pub fn is_safe(&self) -> bool {
        matches!(self, Self::Get | Self::Head | Self::Options | Self::Trace)
    }

    /// is_idempotent returns true if multiple requests with a HTTP method are
    /// intended to have the same effect on the server as a single such request.
    ///
    /// # Examples
    ///
    /// ```
    /// use h1_rs::method::Method;
    /// assert_eq!(Method::Get.is_safe(), true);
    /// assert_eq!(Method::Post.is_safe(), false);
    /// ```
    pub fn is_idempotent(&self) -> bool {
        match self {
            Self::Put | Self::Delete => true,
            _ => self.is_safe(),
        }
    }

    /// is_cacheable indicates that responses to requests with an HTTP method
    /// are allowed to be stored for future reuse.
    ///
    /// # Examples
    ///
    /// ```
    /// use h1_rs::method::Method;
    /// assert_eq!(Method::Get.is_cacheable(), true);
    /// assert_eq!(Method::Put.is_cacheable(), false);
    /// ```
    pub fn is_cacheable(&self) -> bool {
        matches!(self, Self::Get | Self::Head | Self::Post)
    }
}

impl AsRef<str> for Method {
    fn as_ref(&self) -> &str {
        match self {
            Self::Get => "GET",
            Self::Head => "HEAD",
            Self::Post => "POST",
            Self::Put => "PUT",
            Self::Delete => "DELETE",
            Self::Connect => "CONNECT",
            Self::Options => "OPTIONS",
            Self::Trace => "TRACE",
            Self::Patch => "PATCH",
        }
    }
}

impl FromStr for Method {
    type Err = crate::error::Error;

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "GET" => Ok(Method::Get),
            "HEAD" => Ok(Method::Head),
            "POST" => Ok(Method::Post),
            "PUT" => Ok(Method::Put),
            "DELETE" => Ok(Method::Delete),
            "CONNECT" => Ok(Method::Connect),
            "OPTIONS" => Ok(Method::Options),
            "TRACE" => Ok(Method::Trace),
            "PATCH" => Ok(Method::Patch),
            _ => Err(crate::error::Error::InvalidHttpMethod),
        }
    }
}

impl TryFrom<&[u8]> for Method {
    type Error = crate::error::Error;

    #[inline]
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        match value {
            b"GET" => Ok(Method::Get),
            b"HEAD" => Ok(Method::Head),
            b"POST" => Ok(Method::Post),
            b"PUT" => Ok(Method::Put),
            b"DELETE" => Ok(Method::Delete),
            b"CONNECT" => Ok(Method::Connect),
            b"OPTIONS" => Ok(Method::Options),
            b"TRACE" => Ok(Method::Trace),
            b"PATCH" => Ok(Method::Patch),
            _ => Err(crate::error::Error::InvalidHttpMethod),
        }
    }
}
