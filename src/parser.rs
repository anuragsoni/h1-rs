use crate::{
    error::{
        Error::{Fail, InvalidHeaderKey, InvalidHttpVersion, NeedMoreInput},
        Result,
    },
    method::Method,
    request::Request,
    version::Version,
};

fn parse_method(buf: &[u8]) -> Result<(Method, &[u8])> {
    match buf.iter().position(|&ch| ch == b' ') {
        None => Err(NeedMoreInput),
        Some(idx) => {
            let method = buf[0..idx].try_into()?;
            Ok((method, &buf[idx + 1..]))
        }
    }
}

fn parse_token(buf: &[u8]) -> Result<(&str, &[u8])> {
    match buf.iter().position(|&ch| ch == b' ') {
        None => Err(NeedMoreInput),
        Some(idx) => match std::str::from_utf8(&buf[0..idx]) {
            Err(e) => Err(Fail(e.to_string())),
            Ok(token) => Ok((token, &buf[idx + 1..])),
        },
    }
}

fn parse_version(buf: &[u8]) -> Result<(Version, &[u8])> {
    if buf.len() < 8 {
        return Err(NeedMoreInput);
    }

    match &buf[0..8] {
        b"HTTP/1.1" => Ok((Version::Http11, &buf[8..])),
        _ => Err(InvalidHttpVersion),
    }
}

fn parse_eol(buf: &[u8]) -> Result<&[u8]> {
    if buf.len() < 2 {
        return Err(NeedMoreInput);
    }
    match &buf[0..2] {
        b"\r\n" => Ok(&buf[2..]),
        c => Err(Fail(format!(
            "Expected to parse EOL {:?} received {:?}",
            b"\r\n", c
        ))),
    }
}

#[inline]
fn is_tchar(char: u8) -> bool {
    matches!(char, b'0'..=b'9'
        | b'a'..=b'z'
        | b'A'..=b'Z'
        | b'!'
        | b'#'
        | b'$'
        | b'%'
        | b'&'
        | b'\''
        | b'*'
        | b'+'
        | b'-'
        | b'.'
        | b'^'
        | b'_'
        | b'`'
        | b'|'
        | b'~')
}

fn parse_header(buf: &[u8]) -> Result<((&str, &str), &[u8])> {
    match buf.iter().position(|&c| c == b':') {
        None => Err(NeedMoreInput),
        Some(0) => Err(InvalidHeaderKey),
        Some(idx) => {
            if !buf[0..idx].iter().all(|ch| is_tchar(*ch)) {
                return Err(InvalidHeaderKey);
            }
            // We validated the header keys right above here so it should be safe to
            // coerce the ascii values to a str.
            let key = unsafe { std::str::from_utf8_unchecked(&buf[0..idx]) };
            let buf = &buf[idx + 1..];
            match buf.iter().position(|&c| c == b'\r') {
                None => Err(NeedMoreInput),
                Some(idx) => {
                    let value = match std::str::from_utf8(&buf[0..idx]) {
                        Err(e) => return Err(Fail(e.to_string())),
                        Ok(value) => value.trim(),
                    };
                    Ok(((key, value), &buf[idx..]))
                }
            }
        }
    }
}

fn parse_headers(buf: &[u8]) -> Result<(Vec<(&str, &str)>, &[u8])> {
    let mut headers = vec![];
    let mut buf = buf;
    loop {
        if !buf.is_empty() && buf[0] == b'\r' {
            let buf = parse_eol(buf)?;
            return Ok((headers, buf));
        } else {
            let (header, new_buf) = parse_header(buf)?;
            headers.push(header);
            let new_buf = parse_eol(new_buf)?;
            buf = new_buf;
        }
    }
}

pub fn parse_request(buf: &[u8]) -> Result<(Request, usize)> {
    let original_length = buf.len();
    let (method, buf) = parse_method(buf)?;
    let (target, buf) = parse_token(buf)?;
    let (version, buf) = parse_version(buf)?;
    let buf = parse_eol(buf)?;
    let (headers, buf) = parse_headers(buf)?;
    Ok((
        Request {
            method,
            target,
            version,
            headers,
        },
        original_length - buf.len(),
    ))
}
