use crate::{method::Method, version::Version};

#[derive(Debug)]
pub struct Request<'a> {
    pub method: Method,
    pub target: &'a str,
    pub version: Version,
    pub headers: Vec<(&'a str, &'a str)>,
}
